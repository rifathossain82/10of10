import 'dart:convert';

import 'package:active_ecommerce_flutter/app_config.dart';
import 'package:active_ecommerce_flutter/data_model/device_data_model.dart';
import 'package:active_ecommerce_flutter/data_model/device_faults_data_model.dart';
import 'package:active_ecommerce_flutter/data_model/device_version_data_model.dart';
import 'package:http/http.dart' as http;
import 'package:active_ecommerce_flutter/data_model/brand_response.dart';
import 'package:active_ecommerce_flutter/helpers/shared_value_helper.dart';

class MobileReparationRepository {
  Future<DeviceData> getDevices() async {
    Uri url = Uri.parse("${AppConfig.BASE_URL}/reparation");
    final response = await http.get(url, headers: {
      "App-Language": app_language.$,
    });
    return deviceDataFromJson(response.body);
  }

  Future<DeviceVersionData> getDeviceVersions({int deviceId}) async {
    Uri url = Uri.parse("${AppConfig.BASE_URL}/reparation/$deviceId/2");
    final response = await http.get(url, headers: {
      "App-Language": app_language.$,
    });
    return deviceVersionDataFromJson(response.body);
  }

  Future<DeviceFaultsData> getDeviceFaults({int deviceId}) async {
    Uri url = Uri.parse("${AppConfig.BASE_URL}/reparation/$deviceId/3");
    print('You hit: $url');
    final response = await http.get(url, headers: {
      "App-Language": app_language.$,
    });
    return deviceFaultsDataFromJson(response.body);
  }

  Future reparationStore({Map<String, dynamic> body}) async {
    Uri url = Uri.parse("${AppConfig.BASE_URL}/reparation-store");
    print('You hit: $url');
    print('Requested Body: $body');

    final response = await http.post(
      url,
      body: body,
      headers: {
        "App-Language": app_language.$,
      },
    );

    print('Response Status Code: ${response.statusCode}');
    print("Response Body: ${response.body}");

    return response;
  }
}
