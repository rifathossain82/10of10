// To parse this JSON data, do
//
//     final deviceData = deviceDataFromJson(jsonString);

import 'dart:convert';

DeviceData deviceDataFromJson(String str) => DeviceData.fromJson(json.decode(str));

String deviceDataToJson(DeviceData data) => json.encode(data.toJson());

class DeviceData {
  DeviceData({
    this.data,
  });

  List<Device> data;

  factory DeviceData.fromJson(Map<String, dynamic> json) => DeviceData(
    data: List<Device>.from(json["data"].map((x) => Device.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Device {
  Device({
    this.id,
    this.name,
    this.photo,
  });

  int id;
  String name;
  String photo;

  factory Device.fromJson(Map<String, dynamic> json) => Device(
    id: json["id"],
    name: json["name"],
    photo: json["photo"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "photo": photo,
  };
}
