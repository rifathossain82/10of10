// To parse this JSON data, do
//
//     final deviceFaultsData = deviceFaultsDataFromJson(jsonString);

import 'dart:convert';

DeviceFaultsData deviceFaultsDataFromJson(String str) => DeviceFaultsData.fromJson(json.decode(str));

String deviceFaultsDataToJson(DeviceFaultsData data) => json.encode(data.toJson());

class DeviceFaultsData {
  DeviceFaultsData({
    this.version,
    this.item,
    this.services,
  });

  FaultDeviceVersion version;
  Fault item;
  List<Fault> services;

  factory DeviceFaultsData.fromJson(Map<String, dynamic> json) => DeviceFaultsData(
    version: FaultDeviceVersion.fromJson(json["version"]),
    item: Fault.fromJson(json["item"]),
    services: List<Fault>.from(json["services"].map((x) => Fault.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "version": version.toJson(),
    "item": item.toJson(),
    "services": List<dynamic>.from(services.map((x) => x.toJson())),
  };
}

class Fault {
  Fault({
    this.id,
    this.name,
    this.photo,
    this.price,
  });

  int id;
  String name;
  String photo;
  int price;

  factory Fault.fromJson(Map<String, dynamic> json) => Fault(
    id: json["id"],
    name: json["name"],
    photo: json["photo"],
    price: json["price"] == null ? null : json["price"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "photo": photo,
    "price": price == null ? null : price,
  };
}

class FaultDeviceVersion {
  FaultDeviceVersion({
    this.id,
    this.name,
  });

  int id;
  String name;

  factory FaultDeviceVersion.fromJson(Map<String, dynamic> json) => FaultDeviceVersion(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
