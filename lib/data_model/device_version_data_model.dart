import 'dart:convert';

DeviceVersionData deviceVersionDataFromJson(String str) => DeviceVersionData.fromJson(json.decode(str));

String deviceVersionDataToJson(DeviceVersionData data) => json.encode(data.toJson());

class DeviceVersionData {
  DeviceVersionData({
    this.item,
    this.versions,
  });

  Item item;
  List<Version> versions;

  factory DeviceVersionData.fromJson(Map<String, dynamic> json) => DeviceVersionData(
    item: Item.fromJson(json["item"]),
    versions: List<Version>.from(json["versions"].map((x) => Version.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "item": item.toJson(),
    "versions": List<dynamic>.from(versions.map((x) => x.toJson())),
  };
}

class Item {
  Item({
    this.id,
    this.name,
    this.photo,
  });

  int id;
  String name;
  String photo;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"],
    name: json["name"],
    photo: json["photo"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "photo": photo,
  };
}

class Version {
  Version({
    this.id,
    this.name,
  });

  int id;
  String name;

  factory Version.fromJson(Map<String, dynamic> json) => Version(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
