import 'package:active_ecommerce_flutter/my_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SomethingWentWrong extends StatelessWidget {
  const SomethingWentWrong({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: size.height * 0.07),
        Icon(
          Icons.error,
          color: MyTheme.accent_color,
          size: 100,
        ),
        const SizedBox(height: 8),
        Text(
          'Something went wrong!',
          style: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 16,
          ),
        ),
        SizedBox(height: size.height * 0.07),
      ],
    );
  }
}
