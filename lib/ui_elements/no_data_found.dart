import 'package:active_ecommerce_flutter/my_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class NodataFound extends StatelessWidget {
  const NodataFound({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: size.height * 0.07),
        Icon(
          Icons.warning_outlined,
          color: MyTheme.accent_color,
          size: 100,
        ),
        const SizedBox(height: 8),
        Text(
          'No Data Found!',
          style: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 16,
          ),
        ),
        SizedBox(height: size.height * 0.07),
      ],
    );
  }
}
