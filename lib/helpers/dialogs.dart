import 'dart:async';
import 'package:active_ecommerce_flutter/my_theme.dart';
import 'package:flutter/material.dart';

void showCustomDialog({BuildContext context, Widget content}) {
  showDialog(
    context: context,
    builder: (context) {
      Future.delayed(
        Duration(seconds: 3),
        () {
          Navigator.of(context).pop(true);
        },
      );
      return AlertDialog(
        content: content,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        backgroundColor: MyTheme.white,
      );
    },
  );
}
